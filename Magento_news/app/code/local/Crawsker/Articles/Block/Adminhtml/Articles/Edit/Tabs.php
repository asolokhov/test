<?php

class Crawsker_Articles_Block_Adminhtml_Articles_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        $helper = Mage::helper('crawskerarticles');

        parent::__construct();
        $this->setId('articlses_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Articles Information'));
    }

    protected function _prepareLayout()
    {
        $helper = Mage::helper('crawskerarticles');
        
        $this->addTab('general_section', array(
            'label' => $helper->__('General Information'),
            'title' => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('crawskerarticles/adminhtml_articles_edit_tabs_general')->toHtml(),
        ));
        
        $this->addTab('custom_section', array(
            'label' => $helper->__('Custom Fields'),
            'title' => $helper->__('Custom Fields'),
            'content' => $this->getLayout()->createBlock('crawskerarticles/adminhtml_articles_edit_tabs_custom')->toHtml(),
        ));
        return parent::_prepareLayout();
    }

}