<?php

class Crawsker_Articles_Block_Adminhtml_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected function _prepareCollection()
    {
        $this->setCollection(Mage::getModel('crawskerarticles/category')->getCollection());
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('crawskerarticles');

        /*$this->addColumn('category_id', array(
            'header' => $helper->__('Category ID'),
            'index' => 'category_id'
        ));*/

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'name',
            'type' => 'text',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
                    'id' => $model->getId(),
                ));
    }

}