<?php

class Crawsker_Articles_Block_Adminhtml_Category_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        $helper = Mage::helper('crawskerarticles');

        parent::__construct();
        $this->setId('category_tabs');
        $this->setDestElementId('edit_form');
        $this->setName($helper->__('Category Information'));
    }
    
    protected function _prepareLayout()
    {
        $helper = Mage::helper('crawskerarticles');
        $category = Mage::registry('current_category');

        $this->addTab('general_section', array(
            'label' => $helper->__('General Information'),
            'title' => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('crawskerarticles/adminhtml_category_edit_tabs_general')->toHtml(),
        ));
        if($category->getId()){
            $this->addTab('articles_section', array(
                'class' => 'ajax',
                'label' => $helper->__('Articles'),
                'title' => $helper->__('Articles'),
                'url' => $this->getUrl('*/*/articles', array('_current' => true)),
            ));
        }

        return parent::_prepareLayout();
    }

}
