<?php

class Crawsker_Articles_Block_Adminhtml_Category_Edit_Tabs_Articles extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setDefaultFilter(array('ajax_grid_in_category' => 1));
        $this->setId('categoryArticlesGrid');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        //$collection = Mage::registry('current_category')->getArticlesCollection();
        //$this->setCollection($collection);
        //return parent::_prepareCollection();
        $collection = Mage::getModel('crawskerarticles/articles')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('crawskerarticles');

        $this->addColumn('ajax_grid_in_category', array(
            'align' => 'center',
            'header_css_class' => 'a-center',
            'index' => 'id',
            'type' => 'checkbox',
            'values' => $this->getSelectedArticles(),
        ));

        $this->addColumn('ajax_grid_id', array(
            'header' => $helper->__('Articles ID'),
            'index' => 'id',
            'width' => '100px',
        ));

        $this->addColumn('ajax_grid_title', array(
            'header' => $helper->__('Title'),
            'index' => 'title',
            'type' => 'text',
        ));

        $this->addColumn('ajax_grid_created', array(
            'header' => $helper->__('Created'),
            'index' => 'created_time',
            'type' => 'date',
        ));

        return parent::_prepareColumns();
    }
    
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'ajax_grid_in_category') {
            $collection = $this->getCollection();
            $selectedArticles = $this->getSelectedArticles();
            if ($column->getFilter()->getValue()) {
                $collection->addFieldToFilter('id', array('in' => $selectedArticles));
            } elseif (!empty($selectedArticles)) {
                $collection->addFieldToFilter('id', array('nin' => $selectedArticles));
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/articles', array('_current' => true));
    }
    
    public function getSelectedArticles()
    {
        if (!isset($this->_data['selected_articles'])) {
            $selectedArticles = Mage::app()->getRequest()->getParam('selected_articles', null);
            if(is_null($selectedArticles) || !is_array($selectedArticles)){
                $category = Mage::registry('current_category');
                $selectedArticles = $category->getArticlesCollection()->getAllIds();
            }
            $this->_data['selected_articles'] = $selectedArticles;
        }
        return $this->_data['selected_articles'];
    }

}