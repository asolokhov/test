<?php

class Crawsker_Articles_Block_Articles extends Mage_Core_Block_Template
{

    public function getArticlesCollection()
    {
        $articlesCollection = Mage::getModel('crawskerarticles/articles')->getCollection();
        $articlesCollection->setOrder('created_time', 'DESC');
        return $articlesCollection;
    }
    
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

}