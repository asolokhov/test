<?php

class Crawsker_Articles_Model_Category extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('crawskerarticles/category');
    }
    
    protected function _afterDelete()
    {
        foreach($this->getArticlesCollection() as $articles){
            $articles->setCategoryId(0)->save();
        }
        return parent::_afterDelete();
    }

    public function getArticlesCollection()
    {
        $collection = Mage::getModel('crawskerarticles/articles')->getCollection();
        $collection->addFieldToFilter('category_id', $this->getId());
        return $collection;
    }
}