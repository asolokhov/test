<?php

class Crawsker_Articles_Model_Resource_Category extends Mage_Core_Model_Mysql4_Abstract
{

    public function _construct()
    {
        $this->_init('crawskerarticles/table_articles_category', 'category_id');
    }

}