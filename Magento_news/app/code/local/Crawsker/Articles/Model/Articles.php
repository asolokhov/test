<?php

class Crawsker_Articles_Model_Articles extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('crawskerarticles/articles');
    }
    
    protected function _afterDelete()
    {
        $helper = Mage::helper('crawskerarticles');
        @unlink($helper->getImagePath($this->getId()));
        return parent::_afterDelete();
    }

    protected function _beforeSave()
    {
        $helper = Mage::helper('crawskerarticles');
       
        if (!$this->getData('id')) 
        {   
            $uniqueUrlSufix = $helper->getUrlSufix($this->getTitle());
            
            if($uniqueUrlSufix > 1)
            {
                $this->setData('link', $helper->prepareUrl($this->getTitle()).'-'.$uniqueUrlSufix);
            } else
            {
                $this->setData('link', $helper->prepareUrl($this->getTitle()));
            }
        } else 
        {
            $this->setData('link', $helper->prepareUrl($this->getTitle()));
        }
        
        return parent::_beforeSave();
    }
    
    public function getImageUrl()
    {
        $helper = Mage::helper('crawskerarticles');
        if ($this->getId() && file_exists($helper->getImagePath($this->getId()))) {
            return $helper->getImageUrl();
        }
        return null;
    }

}