<?php

class Crawsker_Articles_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function viewAction()
    {
        $articlesId = Mage::app()->getRequest()->getParam('id', 0);
        $articles = Mage::getModel('crawskerarticles/articles')->load($articlesId);

        if ($articles->getId() > 0) {
            $this->loadLayout();
            $this->getLayout()->getBlock('articles.content')->assign(array(
                "articlesItem" => $articles,
            ));
            $this->renderLayout();
        } else {
            $this->_forward('noRoute');
        }
    }

}