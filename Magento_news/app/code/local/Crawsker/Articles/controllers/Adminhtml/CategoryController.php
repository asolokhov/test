<?php

class Crawsker_Articles_Adminhtml_CategoryController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('crawskerarticles');
        $this->_addContent($this->getLayout()->createBlock('crawskerarticles/adminhtml_category'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        $model = Mage::getModel('crawskerarticles/category');

        if ($data = Mage::getSingleton('adminhtml/session')->getFormData()) {
            $model->setData($data)->setId($id);
        } else {
            $model->load($id);
        }
        Mage::register('current_category', $model);

        $this->loadLayout()->_setActiveMenu('crawskerarticles');

        $this->_addLeft($this->getLayout()->createBlock('crawskerarticles/adminhtml_category_edit_tabs'));
        $this->_addContent($this->getLayout()->createBlock('crawskerarticles/adminhtml_category_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        $categoryId = $this->getRequest()->getParam('id');
        if ($data = $this->getRequest()->getPost()) {
            try {
                $helper = Mage::helper('crawskerarticles');
                $model = Mage::getModel('crawskerarticles/category');

                $model->setData($data)->setId($categoryId);
                $model->save();

                $categoryId = $model->getId();
                $categoryArticles = $model->getArticlesCollection()->getAllIds();
                if ($selectedArticles = $this->getRequest()->getParam('selected_articles', null)) {
                    $selectedArticles = Mage::helper('adminhtml/js')->decodeGridSerializedInput($selectedArticles);
                } else {
                    $selectedArticles = array();
                }

                $setCategory = array_diff($selectedArticles, $categoryArticles);
                $unsetCategory = array_diff($categoryArticles, $selectedArticles);

                foreach($setCategory as $id){
                    Mage::getModel('crawskerarticles/articles')->setId($id)->setCategoryId($categoryId)->save();
                }
                foreach($unsetCategory as $id){
                    Mage::getModel('crawskerarticles/articles')->setId($id)->setCategoryId(0)->save();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Category was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $categoryId
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('crawskerarticles/category')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Category was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

    public function articlesAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        $model = Mage::getModel('crawskerarticles/category')->load($id);
        $request = Mage::app()->getRequest();

        Mage::register('current_category', $model);

        if ($request->isAjax()) {

            $this->loadLayout();
            $layout = $this->getLayout();

            $root = $layout->createBlock('core/text_list', 'root', array('output' => 'toHtml'));

            $grid = $layout->createBlock('crawskerarticles/adminhtml_category_edit_tabs_articles');
            $root->append($grid);

            if (!$request->getParam('grid_only')) {
                $serializer = $layout->createBlock('adminhtml/widget_grid_serializer');
                $serializer->initSerializerBlock($grid, 'getSelectedArticles', 'selected_articles', 'selected_articles');
                $root->append($serializer);
            }

            $this->renderLayout();
        }
    }

}