<?php

class Crawsker_Articles_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getImagePath($id = 0)
    {
        $path = Mage::getBaseDir('media') . '/crawskerarticles';
        if ($id) {
            return "{$path}/{$id}.jpg";
        } else {
            return $path;
        }
    }

    public function getImageUrl($id = 0)
    {
        $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'crawskerarticles/';
        if ($id) {
            return $url . $id . '.jpg';
        } else {
            return $url;
        }
    }
    
    public function getCategoriesList()
    {
        $categories = Mage::getModel('crawskerarticles/category')->getCollection()->load();
        $output = array();
        foreach($categories as $category){
            $output[$category->getId()] = $category->getName();
        }
        return $output;
    }

    public function getCategoriesOptions()
    {
        $categories = Mage::getModel('crawskerarticles/category')->getCollection();
        $options = array();
        $options[] = array(
            'label' => '',
            'value' => ''
        );
        foreach ($categories as $category) {
            $options[] = array(
                'label' => $category->getName(),
                'value' => $category->getId(),
            );
        }
        return $options;
    }
    
    public function getStatusList()
    {
        $output = array(1 => 'Активная', 0 => 'Неактивная');
        return $output;
    }
    
    public function getStatusOptions()
    { 
        $options[] = array(
            'label' => 'Неактивная',
            'value' => 0,
        );
        
        $options[] = array(
            'label' => 'Активная',
            'value' => 1,
        );
        
        return $options;
    }
    
    public function prepareUrl($url)
    {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => "'",  'ы' => 'y',   'ъ' => "'",
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
     
            'А' => 'a',   'Б' => 'b',   'В' => 'v',
            'Г' => 'g',   'Д' => 'd',   'Е' => 'e',
            'Ё' => 'e',   'Ж' => 'zh',  'З' => 'z',
            'И' => 'i',   'Й' => 'y',   'К' => 'k',
            'Л' => 'l',   'М' => 'm',   'Н' => 'n',
            'О' => 'o',   'П' => 'p',   'Р' => 'r',
            'С' => 's',   'Т' => 't',   'У' => 'u',
            'Ф' => 'f',   'Х' => 'h',   'Ц' => 'c',
            'Ч' => 'ch',  'Ш' => 'sh',  'Щ' => 'sch',
            'Ь' => "'",   'Ы' => 'y',   'Ъ' => "'",
            'Э' => 'e',   'Ю' => 'yu',  'Я' => 'ya',
            ' ' => '-',
        );
        
        return strtr($url, $converter);
        //return trim(preg_replace('/-+/', '-', preg_replace('/[^a-Z0-9]/sUi', '-', strtolower(trim($url)))), '-');
    }
    
    public function getUrlSufix($title)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "Select MAX(id) AS maxid from crawsker_articles_entities WHERE title = '".$title."'";
        $result       = $connection->fetchOne($sql);
        return $result;
    }
}