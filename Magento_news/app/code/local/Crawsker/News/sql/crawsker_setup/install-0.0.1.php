<?php

$installer = $this;

$installer->startSetup();

$installer->run("
-- Dumping structure for table crawsker_news_entities
DROP TABLE IF EXISTS `{$this->getTable('crawsker_news_entities')}`;
CREATE TABLE `{$this->getTable('crawsker_news_entities')}` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `news_content` MEDIUMTEXT NOT NULL,
  `category_id` INT(11) DEFAULT NULL,
  `link` VARCHAR(255) DEFAULT NULL,
  `meta_title` VARCHAR(255) DEFAULT NULL,
  `meta_keywords` TEXT DEFAULT NULL,
  `meta_description` TEXT DEFAULT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT 1,
  `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping structure for table crawsker_news_category_entities
DROP TABLE IF EXISTS `{$this->getTable('crawsker_news_category_entities')}`;
CREATE TABLE `{$this->getTable('crawsker_news_category_entities')}` (
  `category_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();