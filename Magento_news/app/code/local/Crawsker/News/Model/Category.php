<?php

class Crawsker_News_Model_Category extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('crawskernews/category');
    }
    
    protected function _afterDelete()
    {
        foreach($this->getNewsCollection() as $news){
            $news->setCategoryId(0)->save();
        }
        return parent::_afterDelete();
    }

    public function getNewsCollection()
    {
        $collection = Mage::getModel('crawskernews/news')->getCollection();
        $collection->addFieldToFilter('category_id', $this->getId());
        return $collection;
    }

    /*protected function _afterDelete()
    {
        $newsCollection = Mage::getModel('crawskernews/news')->getCollection()
            ->addFieldToFilter('category_id', $this->getId());
        foreach($newsCollection as $news){
            $news->setCategoryId(0)->save();
        }
        return parent::_afterDelete();
    }*/
}