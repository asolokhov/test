<?php

class Crawsker_News_Model_Resource_Category extends Mage_Core_Model_Mysql4_Abstract
{

    public function _construct()
    {
        $this->_init('crawskernews/table_news_category', 'category_id');
    }

}