<?php

class Crawsker_News_Model_News extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('crawskernews/news');
    }
    
    protected function _afterDelete()
    {
        $helper = Mage::helper('crawskernews');
        @unlink($helper->getImagePath($this->getId()));
        return parent::_afterDelete();
    }

    protected function _beforeSave()
    {
        $helper = Mage::helper('crawskernews');
       
        if (!$this->getData('id')) 
        {   
            $uniqueUrlSufix = $helper->getUrlSufix($this->getTitle());
            
            if($uniqueUrlSufix > 1)
            {
                $this->setData('link', $helper->prepareUrl($this->getTitle()).'-'.$uniqueUrlSufix);
            } else
            {
                $this->setData('link', $helper->prepareUrl($this->getTitle()));
            }
        } else 
        {
            $this->setData('link', $helper->prepareUrl($this->getTitle()));
        }
        
        return parent::_beforeSave();
    }
    
    public function getImageUrl()
    {
        $helper = Mage::helper('crawskernews');
        if ($this->getId() && file_exists($helper->getImagePath($this->getId()))) {
            return $helper->getImageUrl();
        }
        return null;
    }

}