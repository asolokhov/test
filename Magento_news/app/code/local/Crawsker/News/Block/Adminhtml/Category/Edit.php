<?php

class Crawsker_News_Block_Adminhtml_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'crawskernews';
        $this->_controller = 'adminhtml_category';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('crawskernews');
        $model = Mage::registry('current_category');

        if ($model->getId()) {
            return $helper->__("Edit Category '%s'", $this->escapeHtml($model->getName()));
        } else {
            return $helper->__("Add Category");
        }
    }

}