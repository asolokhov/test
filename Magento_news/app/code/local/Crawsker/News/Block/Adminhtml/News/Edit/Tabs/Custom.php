<?php

//class Crawsker_News_Block_Adminhtml_News_Edit_Tabs_Custom extends Mage_Adminhtml_Block_Widget
class Crawsker_News_Block_Adminhtml_News_Edit_Tabs_Custom extends Mage_Adminhtml_Block_Widget_Form
{

    /*protected function _toHtml()
    {
        return '<h2>Custom Fields</h2>';
    }*/
    
    protected function _prepareForm()
    {

        $helper = Mage::helper('crawskernews');
        $model = Mage::registry('current_news');
        
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general_form', array(
                    'legend' => $helper->__('Custom Fields')
                ));
                
        $fieldset->addField('meta_title', 'text', array(
            'label' => $helper->__('Meta_title'),
            //'required' => true,
            'name' => 'meta_title',
        ));
        
        $fieldset->addField('meta_keywords', 'editor', array(
            'label' => $helper->__('Meta_keywords'),
            //'required' => true,
            'name' => 'meta_keywords',
        ));
        
        $fieldset->addField('meta_description', 'editor', array(
            'label' => $helper->__('Meta_description'),
            //'required' => true,
            'name' => 'meta_description',
        ));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

}