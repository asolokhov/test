<?php

class Crawsker_News_Block_Adminhtml_News_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {

        $helper = Mage::helper('crawskernews');
        $model = Mage::registry('current_news');


        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general_form', array(
                    'legend' => $helper->__('General Information')
                ));
                
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(array(
            'add_variables' => false, 
            'add_widgets' => false, 
            'files_browser_window_url' => $this->getBaseUrl() . 'admin/cms_wysiwyg_images/index/'
        ));

        $fieldset->addField('title', 'text', array(
            'label' => $helper->__('Title'),
            'required' => true,
            'name' => 'title',
        ));
        
        $fieldset->addField('image', 'image', array(
            'label' => $helper->__('Image'),
            'name' => 'image',
        ));

        $fieldset->addField('news_content', 'editor', array(
            'name' => 'news_content',
            'label' => $helper->__('News_content'),
            'title' => $helper->__('News_content'),
            'style' => 'width:700px; height:500px;',
            'config' => $wysiwygConfig,
            'wysiwyg' => true,
            'required' => true,
        ));
        
        /*$fieldset->addField('link', 'text', array(
            'label' => $helper->__('Link'),
            'name' => 'link',
        ));*/

        $fieldset->addField('category_id', 'select', array(
            'label' => $helper->__('Category'),
            'name' => 'category_id',
            'values' => $helper->getCategoriesOptions(),
        ));
        
        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'name' => 'status',
            //'value' => 1,
            'values' => $helper->getStatusOptions(),
        ));
        
        /*$fieldset->addField('update_time', 'hidden', array(
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            //'image' => $this->getSkinUrl('images/grid-cal.gif'),
            //'label' => $helper->__('Created'),
            'name' => 'update_time'
        ));*/

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

}