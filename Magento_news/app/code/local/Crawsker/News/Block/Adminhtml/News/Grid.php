<?php

class Crawsker_News_Block_Adminhtml_News_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('crawskernews/news')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('crawskernews');

        /*$this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index' => 'id'
        ));*/

        $this->addColumn('title', array(
            'header' => $helper->__('Title'),
            'index' => 'title',
            'type' => 'text',
        ));
        
        $this->addColumn('category', array(
            'header' => $helper->__('Category'),
            'index' => 'category_id',
            'options' => $helper->getCategoriesList(),
            'type'  => 'options',
            'width' => '150px',
        ));
        
        $this->addColumn('status', array(
            'header' => $helper->__('Status'),
            'index' => 'status',
            'options' => $helper->getStatusList(),
            'type'  => 'options',
            'width' => '150px',
        ));

        $this->addColumn('created_time', array(
            'header' => $helper->__('Created'),
            'index' => 'created_time',
            'type' => 'timestamp',
        ));

        return parent::_prepareColumns();
    }
    
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('news');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
    
    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
                    'id' => $model->getId(),
                ));
    }

}