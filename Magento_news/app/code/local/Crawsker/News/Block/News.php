<?php

class Crawsker_News_Block_News extends Mage_Core_Block_Template
{

    public function getNewsCollection()
    {
        $newsCollection = Mage::getModel('crawskernews/news')->getCollection();
        $newsCollection->setOrder('created_time', 'DESC');
        return $newsCollection;
    }
    
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

}